"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PagSeguro = void 0;
var pagseguro_1 = require("./src/pagseguro");
Object.defineProperty(exports, "PagSeguro", { enumerable: true, get: function () { return __importDefault(pagseguro_1).default; } });
//# sourceMappingURL=index.js.map