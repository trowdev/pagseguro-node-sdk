import { PSAccount, PSItemDetails, PSCheckoutDetails } from './pagseguro.model';
export default class PagSeguro {
    dadosConta: PSAccount;
    itens: PSItemDetails[];
    api: import("axios").AxiosInstance;
    urlPagSeguro: string;
    currency: string;
    reference: string;
    redirectURL: string;
    notificationURL: string;
    constructor(dados: PSAccount);
    setCurrency(cur: string): void;
    addItem(item: PSItemDetails): void;
    setItens(itens: PSItemDetails[]): void;
    setReference(referencia: string): void;
    setRedirectURL(redirectURL: string): void;
    setNotificationURL(notificationURL: string): void;
    validPayment(): boolean;
    buildPayment(): any;
    payment(): Promise<PSCheckoutDetails>;
    transactionStatus(idTransacao: string): Promise<any>;
    notificationStatus(ipnCodigoTransacao: string): Promise<any>;
}
