"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const xml2JSON = require('xml2json-light');
const json2XML = require('json2xml');
class PagSeguro {
    constructor(dados) {
        this.dadosConta = { email: '', token: '' };
        this.itens = [];
        this.urlPagSeguro = '';
        this.currency = '';
        this.reference = '';
        this.redirectURL = '';
        this.notificationURL = '';
        this.dadosConta = dados;
        if (dados.sandbox) {
            this.urlPagSeguro = 'https://ws.sandbox.pagseguro.uol.com.br';
        }
        else {
            this.urlPagSeguro = 'https://ws.pagseguro.uol.com.br';
        }
        this.currency = 'BRL';
        this.itens = [];
        this.api = axios_1.default.create({
            baseURL: this.urlPagSeguro,
            headers: {
                'Content-Type': 'application/xml; charset=UTF-8'
            }
        });
    }
    setCurrency(cur) {
        this.currency = cur;
    }
    addItem(item) {
        item.amount = Number(item.amount).toFixed(2);
        this.itens.push(item);
    }
    setItens(itens) {
        this.itens = itens;
    }
    setReference(referencia) {
        this.reference = referencia;
    }
    setRedirectURL(redirectURL) {
        this.redirectURL = redirectURL;
    }
    setNotificationURL(notificationURL) {
        this.notificationURL = notificationURL;
    }
    validPayment() {
        if (!this.notificationURL) {
            throw new Error('[TrowDev/PagSeguro-SDK] notificationURL is required.');
        }
        else if (!this.redirectURL) {
            throw new Error('[TrowDev/PagSeguro-SDK] redirectURL is required.');
        }
        else if (!this.reference) {
            throw new Error('[TrowDev/PagSeguro-SDK] reference is required.');
        }
        else if (!this.itens || this.itens.length === 0) {
            throw new Error('[TrowDev/PagSeguro-SDK] itens is required.');
        }
        else if (!this.currency) {
            throw new Error('[TrowDev/PagSeguro-SDK] currency is required.');
        }
        return true;
    }
    buildPayment() {
        const data = {};
        if (this.validPayment()) {
            data.items = [];
            this.itens.forEach(item => {
                data['items'].push({ item });
            });
            data['notificationURL'] = this.notificationURL;
            data['redirectURL'] = this.redirectURL;
            data['reference'] = this.reference;
            data['currency'] = this.currency;
        }
        let ret = json2XML({ checkout: data });
        ret = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' + ret;
        return ret;
    }
    payment() {
        return __awaiter(this, void 0, void 0, function* () {
            const dataPayment = this.buildPayment();
            const { email, token } = this.dadosConta;
            const endpoint = `/v2/checkout?email=${email}&token=${token}`;
            const resp = yield this.api.post(endpoint, dataPayment).then(ret => {
                return ret;
            }).catch(err => {
                console.log(err);
            });
            if (!resp || resp.status != 200) {
                console.log('[@trowdev/PagSeguro-SDK] Status Response: ' + resp.status);
                throw new Error('[@trowdev/PagSeguro-SDK] Invalid Response.');
            }
            return xml2JSON.xml2json(resp.data);
        });
    }
    transactionStatus(idTransacao) {
        return __awaiter(this, void 0, void 0, function* () {
            const { email, token } = this.dadosConta;
            const endpoint = `/v3/transactions/${idTransacao}?email=${email}&token=${token}`;
            const resp = yield this.api.get(endpoint).then(ret => {
                return ret;
            }).catch(err => {
                console.log(err);
            });
            if (!resp) {
                throw new Error('[@trowdev/PagSeguro-SDK] Invalid Response.');
            }
            return xml2JSON.xml2json(resp.data);
        });
    }
    notificationStatus(ipnCodigoTransacao) {
        return __awaiter(this, void 0, void 0, function* () {
            const { email, token } = this.dadosConta;
            const endpoint = `/v3/transactions/notifications/${ipnCodigoTransacao}?email=${email}&token=${token}`;
            const resp = yield this.api.get(endpoint).then(ret => {
                return ret;
            }).catch(err => {
                console.log(err);
            });
            if (!resp) {
                throw new Error('[@trowdev/PagSeguro-SDK] Invalid Response.');
            }
            return xml2JSON.xml2json(resp.data);
        });
    }
}
exports.default = PagSeguro;
//# sourceMappingURL=pagseguro.js.map