export interface PSAccount {
    email: string;
    token: string;
    sandbox?: boolean;
}

export interface PSItemDetails {
    id: number,
    description: string,
    amount: string,
    quantity: number,
    weight: number,
}

export interface PSCheckoutDetails {
    checkout: {
        code: string;
        date: string;
    }
}